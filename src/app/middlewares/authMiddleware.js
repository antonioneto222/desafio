const Util = require('../models/Util');
const User = require('../models/User');
var jwtDecode = require('jwt-decode');

module.exports = async (req, res, next) => {
    
    try{
        const authHeader = req.headers.authorization;

        if(!authHeader){
            throw ({"msg":"Não autorizado", "code":-1});
        }
        
        const authHeaderArray = authHeader.split(' ');

        if(authHeaderArray.length !== 2){
            throw ({"msg":"Token inválido", "code":-1}); 
        }

        const [scheme , token] = authHeaderArray;
        
        if(!/^Bearer$/i.test(scheme)){
            throw ({"msg":"Token mal formado", "code":-1}); 
        }
        
        Util.validaToken(token);

        const decoded = jwtDecode(token);

        var usuario = await User.findOne({"id":decoded.id});
        
        if(!usuario){
            throw ({"msg":"Usuário não encontrado", "code":-1}); 
        }

        if(usuario.token != token){
            throw ({"msg":"Não autorizado", "status":401, "code":-1}); 
        }

        // 30 Minutos atras
        const dataLimite = new Date(+(new Date()) - 30 * 60 * 1000);

        const dataToken = new Date(usuario.ultimo_login);

        if(dataLimite > dataToken){
            throw ({"msg":"Sessão inválida", "status":401, "code":-1}); 
        }

        req.usuario = usuario;

        return next();

    }catch(err){
        const resp = Util.trataException(err);
        return res.status(resp.status).send({mensagem: resp.msg});
    }
};