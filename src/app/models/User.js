const Util = require('../models/Util');
const mongoose = require('../../database');

const sha1 = require('sha1');

const aguid = require('aguid');

const UserSchema = new mongoose.Schema({
    id: {
        type: String,
    },
    nome: {
        type: String,
    },
    email: {
        type: String,
    },
    senha: {
        type: String,
    },
    data_criacao: {
        type: Date,
        default: Date.now,
    },
    data_atualizacao:{
        type: Date,
        default: Date.now,
    },
    ultimo_login:{
        type: Date,
        default: Date.now,
    },
    token: {
        type: String,
    },
    telefones: [{ 
                    "numero": {
                        type: String,
                    },
                    "ddd":{
                        type: String,
                    },
                }]
});


UserSchema.pre('save', async function(next){    
    this.id = await aguid();
    this.token = await Util.gerarToken({id:this.id});
    this.senha = await sha1(this.senha);
    next();
});

 
const User = mongoose.model('User', UserSchema);

module.exports = User;