const authConfig = require('../../config/auth');
const jwt = require('jsonwebtoken');


const Util = {
    trataException(e){
        var msg = 'Falha encontrada!';
        var status = 400;

        if(e.code && e.code==-1){
            msg = e.msg;
        }
        if(e.status){
            status = e.status;
        }

        return {msg, status};
    },

    gerarToken(params = {}){
        return jwt.sign( params, authConfig.secret, {
                    expiresIn: 1800
                });
    },

    validaToken(token=""){
       return jwt.sign( token, authConfig.secret, (err) =>{
            if(err)
                throw ({"msg":"Não autorizado", "status":401, "code":-1});
            return true;
        });
         
    }
};


module.exports = Util;