const express = require('express');
const User = require('../models/User');
const Util = require('../models/Util');
const sha1 = require('sha1');


const router = express.Router();


router.post('/sign_up', async(req, res) => {
    try{
        
        const { email } = req.body;

        if(await User.findOne({ email })){
            throw ({"msg":"E-mail já existente", "code":-1});
        }
        
        const usuario = await User.create(req.body);
        
        return res.status(200).send({usuario});
        
    }catch(err){
        const resp = Util.trataException(err);
        return res.status(resp.status).send({mensagem: resp.msg});
    }
});


router.post('/sign_in', async(req, res) => {
    try{
        
        const { email } = req.body;
        const { senha } = req.body;
        
        var usuario = await User.findOne({"email":email});
        
        // Email não encontrado
        if(!usuario){
            throw ({"msg":"Usuário e/ou senha inválidos", "code":-1});
        }
        
        // Email existe, no entanto a senha não bate        
        if(usuario.senha != await sha1(senha)){
            throw ({"msg":"Usuário e/ou senha inválidos", "status":401, "code":-1});
        }
        
        usuario.token = Util.gerarToken({id:usuario.id});
        const dateNow = new Date();
        User.findOneAndUpdate({"id":usuario.id}, {"token":usuario.token, "ultimo_login":dateNow, "data_atualizacao":dateNow}, {upsert:true}, function(err, res){
            if (err) 
                throw ({"msg":"Falha ao atualizar token", "status":500, "code":-1});
        });

        // Email exist e a senha bateu 
        return res.status(200).send({usuario});
        
    }catch(err){
        const resp = Util.trataException(err);
        return res.status(resp.status).send({mensagem: resp.msg});
    }
});


module.exports = app => app.use('/auth', router);