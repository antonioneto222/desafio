const express = require('express');
const authMiddleware = require('../middlewares/authMiddleware');

const router = express.Router();

router.use(authMiddleware);

router.post('/buscar', (req, res) => {
    return res.status(200).send(req.usuario);
});
 

module.exports = app => app.use('/app', router);